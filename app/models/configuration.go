package models

import (
	"encoding/json"
	"time"
)

type streamConfiguration struct {
	Name			string `json:"name"`
	Location  string `json:"location"`
	Type      string `json:"type"`
	Width     int32  `json:"width"`
	Height    int32  `json:"height"`
	StartTime int32  `json:"start"`
	StopTime  int32  `json:"stop"`
}

type chatConfiguration struct {
	Server  string `json:"server"`
	Port    int32  `json:"port"`
	Channel string `json:"channel"`
}

type configuration struct {
	Stream      []streamConfiguration `json:"stream"`
	Chat        chatConfiguration     `json:"chat"`
	Theme       string                `json:"theme"`
	StrawPollId string                `json:"strawPollId"`
	UpdateTime	int32								 `json:"updateTime"`
}

func (config configuration) Update(newConfig string, setTime bool) error {
	connection := Database.Get()
	defer connection.Close()

	if jsonDecodeErr := json.Unmarshal([]byte(newConfig), &config); jsonDecodeErr != nil {
		return jsonDecodeErr
	}

	if setTime {
		config.UpdateTime = int32(time.Now().Unix())
	}

	encodedConfig, jsonEncodeErr := json.Marshal(config)
	if jsonEncodeErr != nil {
		return jsonEncodeErr
	}

	_, updateErr := connection.Do("SET", "dctv:configuration", string(encodedConfig))
	return updateErr
}

func CurrentConfiguration() (*configuration, error) {
	connection := Database.Get()
	defer connection.Close()

	config := &configuration{
		Stream: []streamConfiguration{
			streamConfiguration{
				Location: "scamschoolbrian",
				Type:     "justintv",
				Width:    1080,
				Height:   720,
				StartTime: 0,
				StopTime: 0,
			},
		},
		Chat: chatConfiguration{
			Server:  "irc.diamondclub.tv",
			Port:    6667,
			Channel: "chat",
		},
		Theme:       "",
		StrawPollId: "1",
	}

	encodedConfig, getErr := Database.String(connection.Do("GET", "dctv:configuration"))
	if getErr != nil {
		return nil, getErr
	}

	if jsonDecodeErr := json.Unmarshal([]byte(encodedConfig), &config); jsonDecodeErr != nil {
		return nil, jsonDecodeErr
	}

	return config, nil
}
