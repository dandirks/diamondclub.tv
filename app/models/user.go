package models

import (
	"encoding/json"
)

const (
	StandardUser int = iota
	ModeratorUser
	AdministratorUser
)

type User struct {
	Username       string `json:"username"`
	Access         int    `json:"access"`
	HashedPassword string `json:"hashedPassword"`
	SessionId      string `json:"sessionId"`
}

func (u *User) GetByUsername(username string) (*User, error) {
	connection := Database.Get()
	defer connection.Close()

	encodedUser, getErr := Database.String(connection.Do("HGET", "dctv:users", username))
	if getErr != nil {
		return nil, getErr
	}

	if jsonErr := json.Unmarshal([]byte(encodedUser), &u); jsonErr != nil {
		return nil, jsonErr
	}
	return u, nil
}

func (u *User) GetBySessionId(sessionId string) (*User, error) {
	connection := Database.Get()
	defer connection.Close()

	username, getUsernameErr := Database.String(connection.Do("HGET", "dctv:sessions", sessionId))
	if getUsernameErr != nil {
		return nil, getUsernameErr
	}

	return u.GetByUsername(username)
}

func (u *User) ActivateSession(sessionId string) error {
	connection := Database.Get()
	defer connection.Close()

	if u.SessionId != "" {
		u.DeactivateSession()
	}

	if _, err := connection.Do("HSET", "dctv:sessions", sessionId, u.Username); err != nil {
		return err
	}
	u.SessionId = sessionId
	return u.Save()
}

func (u *User) DeactivateSession() error {
	connection := Database.Get()
	defer connection.Close()

	_, err := connection.Do("HDEL", "dctv:sessions", u.SessionId)

	return err
}

func (u *User) Save() error {
	connection := Database.Get()
	defer connection.Close()

	encodedUser, encodingErr := json.Marshal(u)
	if encodingErr != nil {
		return encodingErr
	}

	_, updateErr := connection.Do("HSET", "dctv:users", u.Username, string(encodedUser))
	return updateErr
}
