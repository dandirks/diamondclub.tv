package models

import (
	"github.com/garyburd/redigo/redis"
	"time"
)

var pool = &redis.Pool{
	MaxIdle:     3,
	IdleTimeout: 240 * time.Second,
	Dial: func() (redis.Conn, error) {
		c, err := redis.Dial("tcp", ":6379")
		if err != nil {
			return nil, err
		}
		return c, err
	},
	TestOnBorrow: func(c redis.Conn, t time.Time) error {
		_, err := c.Do("PING")
		return err
	},
}

type database struct{}

func (d database) Get() redis.Conn {
	return pool.Get()
}

func (d database) String(reply interface{}, err error) (string, error) {
	return redis.String(reply, err)
}

var Database = &database{}
