package controllers

import (
	"code.google.com/p/go.crypto/bcrypt"
	"diamondclub.tv/app/models"
	"github.com/robfig/revel"
)

type Authentication struct {
	*revel.Controller
	User *models.User
}

func (c *Authentication) AuthenticateRequest() revel.Result {
	c.User, _ = (&models.User{}).GetBySessionId(c.Session.Id())
	return nil
}

// AUTHENTICATION ENDPOINTS
func (c Authentication) Login(username, password string) revel.Result {
	user, err := (&models.User{}).GetByUsername(username)
	if err != nil {
		c.Response.Status = 400
	} else if err := bcrypt.CompareHashAndPassword([]byte(user.HashedPassword), []byte(password)); err != nil {
		c.Response.Status = 400
	} else {
		user.ActivateSession(c.Session.Id())
		json := make(map[string]interface{})
		json["username"] = user.Username
		json["access"] = user.Access
		return c.RenderJson(json)
	}

	return c.RenderText("")
}

func (c Authentication) Logout() revel.Result {
	if c.User == nil {
		c.Response.Status = 400
	} else {
		c.User.DeactivateSession()
	}

	return c.RenderText("")
}

func (c Authentication) Register(username, password string, access int) revel.Result {
	if c.User == nil || c.User.Access < models.AdministratorUser {
		c.Response.Status = 401
	} else if _, userGetErr := (&models.User{}).GetByUsername(username); userGetErr == nil {
		c.Response.Status = 400
	} else {
		hashedPassword, hashErr := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
		if hashErr != nil {
			c.Response.Status = 500
		} else {
			newUser := &models.User{}
			newUser.Username = username
			newUser.HashedPassword = string(hashedPassword)
			newUser.Access = access
			if saveErr := newUser.Save(); saveErr != nil {
				c.Response.Status = 500
			}
		}
	}
	return c.RenderText("")
}

func (c Authentication) WhoAmI() revel.Result {
	if c.User == nil {
		c.Response.Status = 400
		return c.RenderText("")
	}

	json := make(map[string]interface{})
	json["username"] = c.User.Username
	json["access"] = c.User.Access
	return c.RenderJson(json)
}

func (c Authentication) UpdatePassword(password string) revel.Result {
	if c.User == nil {
		c.Response.Status = 400
	} else {
		hashedPassword, hashErr := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
		if hashErr != nil {
			c.Response.Status = 400
		} else {
			c.User.HashedPassword = string(hashedPassword)
			if saveErr := c.User.Save(); saveErr != nil {
				c.Response.Status = 400
			}
		}
	}

	return c.RenderText("")
}

func init() {
	revel.InterceptMethod((*Authentication).AuthenticateRequest, revel.BEFORE)
}
