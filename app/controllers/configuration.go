package controllers

import (
	"diamondclub.tv/app/models"
	"github.com/robfig/revel"
	"io/ioutil"
)

type Configuration struct {
	*revel.Controller
	Authentication
}

// CONFIGURATION
func (c Configuration) GetConfiguration() revel.Result {
	config, configErr := models.CurrentConfiguration()
	if configErr != nil {
		c.Response.Status = 500
		return c.RenderText("")
	}
	return c.RenderJson(config)
}

func (c Configuration) UpdateConfiguration(quietUpdate bool) revel.Result {
	if c.User == nil || c.User.Access < models.ModeratorUser {
		c.Response.Status = 401
	} else {
		body, bodyErr := ioutil.ReadAll(c.Request.Body)
		if bodyErr != nil {
			c.Response.Status = 400
		} else {
			config, configErr := models.CurrentConfiguration()
			if configErr != nil {
				c.Response.Status = 500
			} else if updateErr := config.Update(string(body), !quietUpdate); updateErr != nil {
				c.Response.Status = 500
			} else {
				c.Response.Status = 201
			}
		}
	}

	return c.RenderText("")
}
