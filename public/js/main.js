angular.module('notification', ['ngSanitize']).
  service('notification', function() {
    var data = this._data = {
      message: null,
      prompt: null,
      value: null,
      buttons: null
    };

    this.show = function(message) {
      data.message = message;
      data.prompt = false;
    };
    this.prompt = function(message, value, buttons) {
      data.prompt = true;
      data.message = message;
      data.value = value;
      data.buttons = buttons;
    };
    this.dismiss = function() {
      data.message = null;
      data.prompt = null;
      data.value = null;
      data.buttons = null;
    };
  }).
  directive('notification', function(notification) {
    return {
      templateUrl: '/views/partials/notification.html',
      replace: true,
      scope: true,
      link: function(scope) {
        scope.notification = notification;
        scope.$on('$routeChangeSuccess', scope.notification.dismiss);
      }
    };
  });

angular.module('DiamondclubTV', ['ngRoute', 'notification'])
  .config(['$routeProvider', '$locationProvider', '$sceDelegateProvider', function($routeProvider, $locationProvider, $sceDelegateProvider) {
    $routeProvider.when('/', {
      templateUrl: '/views/home.html',
      controller: 'HomeCtrl'
    });

    $routeProvider.when('/chat', {
      templateUrl: '/views/chat.html',
      controller: 'HomeCtrl'
    });

    $routeProvider.when('/admin', {
      templateUrl: '/views/admin.html',
      controller: 'AdminCtrl'
    });

    $routeProvider.otherwise({redirectTo: '/'});

    $locationProvider.html5Mode(true);

    $sceDelegateProvider.resourceUrlWhitelist([
      'self',
      'about:blank',
      'http://strawpoll.me/embed_2/**',
      'http://www.justin.tv/**',
      'http://www.ustream.tv/embed/**',
      'http://www.youtube.com/embed/**',
      'http://www.dailymotion.com/embed/**',
      'http://webchat.twit.tv/**',
      'http://irc.cordkillers.com/**',
      'http://www.metacdn.com/**'
    ]);
  }]);


angular.module('DiamondclubTV')
  .controller('HomeCtrl', ['$scope', 'Configuration', function($scope, Configuration) {
    var nextUpdate = null,
        playlistPadTime = 5;

    Configuration.get().then(function(configuration) {
      $scope.configuration = configuration;
    });
    Configuration.updateOnChanges();

    var getPlaylistLength = function(playlist) {
      return _.reduce(playlist, function(seconds, playlistItem) {
        return seconds + playlistItem.stop + playlistPadTime;
      }, 0);
    }

    var updatePlaylistItem = function() {
      clearTimeout(nextUpdate);

      var currentTime = (Date.now() / 1000),
          updateTime = $scope.configuration.updateTime,
          playlistLength = getPlaylistLength($scope.configuration.stream);

      updateTime += playlistLength * Math.floor((currentTime - updateTime) / playlistLength);

      var item = _.cloneDeep(_.find($scope.configuration.stream, function(playlistItem) {
        if(playlistItem.stop === 0 || currentTime >= updateTime && currentTime <= (updateTime + playlistItem.stop + playlistPadTime)) {
          return true;
        }
        updateTime += playlistItem.stop + playlistPadTime;
        return false;
      }));

      item.start = (currentTime - updateTime) + item.start;

      if(item.stop) {
        setTimeout(updatePlaylistItem, Math.ceil((item.stop - item.start + playlistPadTime) * 1000));
      }

      $scope.playlistItem = item;
      if(!$scope.$$phase) {
        $scope.$apply();
      }
    };

    $scope.getStreamLocation = function(playlistItem) {
      if(!playlistItem || !playlistItem.location || !playlistItem.type) {
        return 'about:blank';
      }

      switch(playlistItem.type) {
        case 'justintv':
          return 'http://www.justin.tv/swflibs/JustinPlayer.swf?channel=' + playlistItem.location;
        case 'twitch':
          return 'http://www.twitch.tv/widgets/live_embed_player.swf?channel=' + playlistItem.location;
        case 'youtube-live':
          return '//www.youtube.com/embed/' + playlistItem.location + '?autoplay=1';
        case 'youtube':
          return '//www.youtube.com/embed/' + playlistItem.location + '?html5=1&autoplay=1&start=' + Math.floor(playlistItem.start);
        case 'ustream':
          return 'http://www.ustream.tv/embed/' + playlistItem.location + '?v=3&wmode=direct';
        case 'dailymotion':
          return 'http://www.dailymotion.com/embed/video/' + playlistItem.location + '?autoplay=1';
        case 'metacdn':
          return 'http://www.metacdn.com/r/l/' + playlistItem.location + '/embed';
        default:
          return playlistItem.location;
      }
    };

    $scope.getChatLocation = function(configuration) {
      if(!configuration || !configuration.chat || !configuration.chat.server || !configuration.chat.channel) {
        return 'about:blank';
      }

      return 'http://' + configuration.chat.server + '/?channels=' + configuration.chat.channel;
    };

    $scope.$watchCollection('configuration.stream', function(newValue) {
      if(newValue) {
        updatePlaylistItem();
      }
    });

  }]);


angular.module('DiamondclubTV')
  .controller('AdminCtrl', ['$scope', '$timeout', 'User', 'Configuration', 'notification', function($scope, $timeout, User, Configuration, notification) {
    var defaultsDeep = _.partialRight(_.merge, _.defaults),
        defaultConfiguration = {
          name: "New playlist item",
          location: "",
          type: "youtube",
          width: 1080,
          height: 720
        };

    Configuration.get().then(function(configuration) {
      $scope.configuration = configuration;
    });

    $scope.user = User.getCurrent();
    $scope.streamTypes = ['justintv', 'twitch', 'youtube', 'youtube-live', 'ustream', 'dailymotion', 'metacdn'];
    $scope.presets = [
      defaultsDeep({
        _presetName: 'NAP MetaCDN',
        name: 'NAP MetaCDN',
        location: 'bdhftdlxs/nightattack',
        type: 'metacdn'
      }, defaultConfiguration),
      defaultsDeep({
        _presetName: 'Brian\'s Justin.TV',
        name: 'Brian\'s Justin.TV',
        location: 'scamschoolbrian',
        type: 'justintv'
      }, defaultConfiguration),
      defaultsDeep({
        _presetName: 'Justin\'s Justin.TV',
        name: 'Justin\'s Justin.TV',
        location: 'justinrobertyoung',
        type: 'justintv'
      }, defaultConfiguration),
      defaultsDeep({
        _presetName: 'Brian\'s Ustream',
        name: 'Brian\'s Ustream',
        location: '390279',
        type: 'ustream'
      }, defaultConfiguration),
      defaultsDeep({
        _presetName: 'Brian\'s Dailymotion',
        name: 'Brian\'s Dailymotion',
        location: 'x1a389a',
        type: 'dailymotion'
      }, defaultConfiguration)
    ];

    $scope.newPlaylistItem = function() {
      $scope.configuration.stream.push(_.cloneDeep(defaultConfiguration));
    };

    $scope.removePlaylistItem = function(item) {
      var idx = $scope.configuration.stream.indexOf(item);
      if(idx !== -1) {
        $scope.configuration.stream.splice(idx, 1);
      }
    };

    $scope.movePlaylistItem = function(item, newIdx) {
      var idx = $scope.configuration.stream.indexOf(item);
      if(idx !== -1) {
        if(newIdx == $scope.configuration.stream.length - 1) {
          $scope.configuration.stream.push($scope.configuration.stream.splice(idx, 1)[0]);
        } else {
          $scope.configuration.stream.splice(newIdx, 0, $scope.configuration.stream.splice(idx, 1)[0]);
        }
      }
    };

    $scope.randomizePlaylist = function() {
      $scope.configuration.stream = _.shuffle($scope.configuration.stream);
    };

    $scope.exportPlaylist = function() {
      notification.prompt('To export, save the data in the text box. This will be used to import this playlist.', angular.toJson($scope.configuration.stream));
    };

    $scope.importPlaylist = function() {
      notification.prompt('Enter the exported playlist in the text box.', null, [{
          text: 'Import',
          primary: true,
          callback: function(value) {
            try {
              $scope.configuration.stream = JSON.parse(value);
              notification.dismiss();
            } catch(e) {
              alert('Error: Could not parse the playlist data.');
            }
          }
        }, {
          text: 'Cancel',
          callback: notification.dismiss
        }
      ]);
    }

    $scope.copyConfiguration = function(playlistItem, configuration) {
      _.merge(playlistItem, configuration);
    };

    $scope.login = function(username, password) {
      $scope.loginError = null;
      User.authenticate(username, password, function(err, currentUser) {
        $scope.loginError = err;
        $scope.user = currentUser;
      });
    };

    $scope.logout = function() {
      User.logout();
    };

    $scope.applyConfiguration = function(configuration) {
      $scope.applyConfigurationError = null;
      delete configuration.updateTime;
      Configuration.set(configuration, configuration.quiet || false, function(err) {
        $scope.applyConfigurationError = err;
        if(!err) {
          $timeout(function() { $scope.applyConfigurationError = null; }, 5000);
        }
      });
    };

  }]);

angular.module('DiamondclubTV')
  .service('Configuration', ['$http', '$rootScope', '$q', function($http, $rootScope, $q) {
    var currentConfiguration = {},
        socket = null;

    this.get = function() {
      var deferred = $q.defer();

      $http.get('/api/configuration')
        .success(function(data) {
          _.extend(currentConfiguration, data);
          deferred.resolve(currentConfiguration);
        })
        .error(function() {
          console.error('could not load configuration');
          deferred.reject();
        });

      return deferred.promise;
    };

    this.set = function(configuration, quiet, callback) {
      $http.put('/api/configuration', configuration, { params: { quietUpdate: quiet }})
        .success(callback.bind(null, false))
        .error(callback.bind(null, true));
    };

    this.updateOnChanges = function() {
      if(socket) {
        return socket;
      }

      socket = new WebSocket('ws://updates.diamondclub.tv/ws/front_page_updates');

      socket.onerror = function (error) {
        console.log('WebSocket Error ' + error);
      };

      socket.onmessage = function (e) {
        var data = JSON.parse(e.data);
        _.merge(currentConfiguration, data);
        $rootScope.$digest();
      };
    };

    this.stopUpdateOnChanges = function() {
      socket.close();
      socket = null;
    };
  }]);

angular.module('DiamondclubTV')
  .service('User', ['$http', function($http) {
    var currentUser = {};

    var updateCurrentUser = function(data) {
      if(data.username) {
        currentUser.username = data.username;
        currentUser.access = data.access;
      }
      return currentUser;
    };

    var resetCurrentUser = function() {
      currentUser.username = undefined;
      currentUser.access = undefined;
      return currentUser;
    };

    this.getCurrent = function() {
      $http.get('/user')
        .success(updateCurrentUser)
        .error(resetCurrentUser);
      return currentUser;
    };

    this.authenticate = function(username, password, callback) {
      var bodyString = 'username=' + encodeURIComponent(username) + '&password=' + encodeURIComponent(password);
      $http.post('/user/login', bodyString, { headers: {'Content-Type': 'application/x-www-form-urlencoded'} })
        .success(function(data) {
          callback(false, updateCurrentUser(data));
        })
        .error(function() {
          callback(true, resetCurrentUser());
        })
    };

    this.logout = function() {
      $http.post('/user/logout')
        .success(resetCurrentUser);
    };
  }]);

angular.module('DiamondclubTV')
  .directive('dcPopout', ['$timeout', function($timeout) {
    return {
      restrict: 'A',
      replace: true,
      transclude: true,
      templateUrl: '/views/partials/popout.html',
      link: function($scope, element, attrs) {
        var windowFeatures = 'menubar=no,location=no,resizable=yes,scrollbars=yes,status=no';
        $scope.popoutOpen = false;

        $scope.popout = function(event) {
          var sectionElement = element.next();

          var url = $scope.$eval(attrs.dcPopout),
              features = windowFeatures + ',height=' + sectionElement[0].clientHeight + ',width=' + sectionElement[0].clientWidth,
              childWindow = window.open(url, '<>.tv :: ' + url, features);

          childWindow.window.onbeforeunload = function() {
            $timeout(function() {
              $scope.popoutOpen = false;
            });
          };

          $scope.popoutOpen = true;
          event.stopPropagation();
          event.preventDefault();
        };
      }
    }
  }]);
